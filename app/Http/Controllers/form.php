<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class form extends Controller
{
    function index(Request $request){
        
        $request->validate([
            'name'=>'required | min:3|max:10',
            'email'=>'required | email',
            'doc'=>'required |mimes:jpeg,png'
        ]);
        

       echo $request->file('doc')->store('media');
        return $request->post();
    }
    
}
