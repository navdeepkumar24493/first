<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class User1 extends Controller
{
    // for Session set
    function session_set(Request $r)
    {
        $r->session()->put('name', 'Vishal');
    }
    // for Session get
    function session_get(Request $r)
    {
        echo $r->session()->get('name');
    }
    // for Session remove
    function session_remove(Request $r)
    {
        echo $r->session()->forget('name');
    }
    // for Session check
    function session_check(Request $r)
    {
        if ($r->session()->has('name')) {
            echo "yes";
        } else {
            echo "no";
        }
    }

    function Userlogin(Request $r)
    {
        $r->validate([
            'password'=>'required | min:3|max:10',
            'email'=>'required | email',]);
            $email=$r->input('email');
            $password=$r->input('password');

           if($email=='a@gmail.com' && $password=='123'){
           $r->session()->put('name','vishal');
           return redirect('news1');
           }
           else{
           $r->session()->flash('error','Please enter valid login details');
           return redirect('login');
           }
            
    }
    
}
