<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\form;
use App\Http\Controllers\User1;
use App\Http\Controllers\DB_test;
use App\Http\Controllers\Userprofile;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// route::view('first','home');
Route::get('/first/{id}', function ($id) {
    echo $id;
    return view('home');
});

route::get("user",[UserController::class,'index']);

Route::get('/page', function () {
    return view('home');
});

Route::view('form','form');
 Route::post('FormSubmit',[form::class,'index']);

Route::get('/web', function () {
    return view('web',array('data'=>array('vishal','amit','rahul')));
});

// Route::view('news1','news1')->middleware('UserCheck');
// Route::view('news2','news2');

route::view('denied','denied');

Route::group(['middleware'=>['UserCheck']],function(){
    Route::view('news1','news1');
    Route::view('news2','news2');

});

route::view('submit','formvalidate');
route::post('FormSubmited',[FormSubmited::class,'index']);

// Session

// for Session set
Route::get('session_set',[User1::class,'session_set']);
// for Session get
Route::get('session_get',[User1::class,'session_get']);
// for Session remove
Route::get('session_remove',[User1::class,'session_remove']);
// for Session check
Route::get('session_check',[User1::class,'session_check']);


// loginform

Route::view('login','login');
Route::post('formSubmit1',[User1::class,'Userlogin']);

// Select query
Route::get('select',[DB_test::class,'select']);
// insert query
Route::get('insert',[DB_test::class,'insert']);
// update query
Route::get('update',[DB_test::class,'update']);
// delete query
Route::get('delete',[DB_test::class,'delete']);


Route::get('get_data',[Userprofile::class,'index']);