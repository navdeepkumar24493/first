<h1>Form</h1>
<form method="POST" action="formSubmit1" >
    {{@csrf_field()}}
    <table border='2px'>
      
        <tr>
            <td>Email :</td>
            <td>
                <input type="text" name="email">
                <br>
                <span class="field_error">
                    @error('email')

                    {{$message}}
                    @enderror
                </span>
            </td>
        </tr>
        <tr>
            <td>Password :</td>
            <td>
                <input type="password" name="password">
                <br>
                <span class="field_error">
                    @error('password')

                    {{$message}}
                    @enderror
                </span>
            </td>
        </tr>
        
        <tr>

            <td colspan="2">
                <input type="submit" name="submit" value="Submit">
                <br>
                {{session('error')}}
            </td>
        </tr>


    </table>


</form>
<style>
    .field_error {
        background-color: wheat;
        color: red;
    }
</style>