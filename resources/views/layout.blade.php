<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
</head>
<body>
    <h1>Main template</h1>
    <div id="header">
    <h2>Header section</h2>
    </div>
    <div id="container">
    @section('container')
    
    @show
    </div>
    <div id="footer">
    <h2>footer section</h2>
    </div>
</body>
</html>