<h1>Form</h1>
<form method="POST" action="FormSubmit" enctype="multipart/form-data">
    {{@csrf_field()}}
    <table border='2px'>
        <tr>
            <td>Name :</td>
            <td>
                <input type="text" name="name">
                <br>
                <span class="field_error">
                    @error('name')

                    {{$message}}
                    @enderror
                </span>
            </td>
        </tr>
        <tr>
            <td>Email :</td>
            <td>
                <input type="text" name="email">
                <br>
                <span class="field_error">
                    @error('email')

                    {{$message}}
                    @enderror
                </span>
            </td>
        </tr>
        <tr>
            <td>Image Upload :</td>
            <td>
                <input type="file" name="doc">
                <br>
                <span class="field_error">
                    @error('doc')

                    {{$message}}
                    @enderror
                </span>

            </td>
        </tr>
        <tr>

            <td colspan="2">
                <input type="submit" name="submit" value="Submit">
            </td>
        </tr>


    </table>


</form>
<style>
    .field_error {
        background-color: wheat;
        color: red;
    }
</style>